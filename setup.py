from setuptools import setup

setup(version='0.0.2',
      name='talkbank_parser',
      description='Tools for parsing Talkbank XML corpora.',
      url='https://bitbucket.org/pfeyz/talkbank-parser.git',
      author='Paul Feitzinger',
      packages=['talkbank_parser'])
